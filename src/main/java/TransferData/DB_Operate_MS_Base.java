package TransferData;

import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.dbcp2.BasicDataSource;

public abstract class DB_Operate_MS_Base {
    private final String DRIVER;
    private static final Object synced = new Object();
    private org.apache.commons.dbcp2.BasicDataSource datasource = null;
    private int maxIdlePool = -1, minIdelPool = 50, maxTotalPool = 100;
    private Boolean isInitializing = false;

    protected abstract String GetConnectString() throws Exception;

    public DB_Operate_MS_Base(String driver) throws ClassNotFoundException {
        Class.forName(driver);
        this.DRIVER = driver;
    }

    public DB_Operate_MS_Base(String driver, int maxIdlePool, int minIdelPool, int maxTotalPool) throws Exception {
        Class.forName(driver);
        this.DRIVER = driver;
        this.maxIdlePool = maxIdlePool;
        this.minIdelPool = minIdelPool;
        this.maxTotalPool = maxIdlePool;
    }

    private void InitPool() throws Exception {
        if (datasource == null) {
            synchronized (synced) {
                try {
                    if (datasource == null) {
                        isInitializing = true;
                        datasource = new BasicDataSource();
                        String connectString = GetConnectString();
                        // PoolProperties p = new PoolProperties();
                        // p.setUrl(connectString);
                        // p.setDriverClassName(ServerType);
                        // p.setMaxActive(20);
                        // p.setMinIdle(2);
                        // p.setMaxIdle(5);
                        // datasource.setPoolProperties(p);
                        datasource.setDriverClassName(DRIVER);
                        datasource.setMaxIdle(maxIdlePool); // Pool中最大的空閒的連線數量，也就是所能容納狀態為idle的DB Connection的最大數量，-1 表示無上限
                        datasource.setMinIdle(minIdelPool); // Pool中最小的空閒的連線數量
                        datasource.setMaxTotal(maxTotalPool); // Pool在同一時刻內所提供的最大活動連接數
                        datasource.setUrl(connectString);
                        System.out.println("InitPool：" + connectString);
                        isInitializing = false;
                    }
                } catch (Exception e) {
                    datasource = null;
                    isInitializing = false;
                    throw e;
                }
            }
        }
    }

    /**
     * 查詢(SELECT)
     */
    public List<Map<String, Object>> ExecuteQuery(String sqlquery, List<Object> paramList) throws Exception {
        long startTime = System.currentTimeMillis();
        ResultSet resultSet = null;
        InitPool();
        Connection connection = null;
        PreparedStatement pStatement = null;
        List<Map<String, Object>> result = new ArrayList<>();
        try {
            connection = datasource.getConnection();
            pStatement = connection.prepareStatement(sqlquery);
            System.out.println("執行的SQL語法：" + sqlquery);
            resultSet = (ResultSet) Execute(paramList, false, pStatement);
            if (resultSet != null) {
                int columnCount = resultSet.getMetaData().getColumnCount();
                List<String> columnNameList = new ArrayList<>();
                for (int i = 1; i <= columnCount; i++) {
                    columnNameList.add(resultSet.getMetaData().getColumnName(i));
                }

                while (resultSet.next()) {
                    Map<String, Object> datakeyvalue = new HashMap<String, Object>();
                    for (String s : columnNameList) {
                        Object o = resultSet.getObject(s);
                        datakeyvalue.put(s, o);
                    }
                    result.add(datakeyvalue);
                }
            } else
                throw new Exception("查詢資料回傳NULL，請檢查SQL 語法或該方法 SQL：" + sqlquery);
        } catch (SQLException e) {
            if (e.getErrorCode() == 0 && !isInitializing) {
                datasource = null;
                InitPool();
            }
            e.printStackTrace();
            throw e;
        } finally {
            Close(resultSet, connection, pStatement);
        }
        long endTime = System.currentTimeMillis();
        long totTime = endTime - startTime;
        System.out.println("SQL執行時間:" + totTime + " 資料筆數：" + result.size());
        return result;
    }

    /**
     * 查詢(非SELECT)
     */
    public int ExecuteNonQuery(String sqlquery, List<Object> paramList) throws Exception {
        Object result = -1;
        InitPool();
        Connection connection = null;
        PreparedStatement pStatement = null;
        try {
            connection = datasource.getConnection();
            pStatement = connection.prepareStatement(sqlquery);
            System.out.println("執行的SQL語法：" + sqlquery);
            result = Execute(paramList, true, pStatement);
        } catch (SQLException e) {
            if (e.getErrorCode() == 0 && !isInitializing) {
                datasource = null;
                InitPool();
            }
            e.printStackTrace();
            throw e;
        } finally {
            Close(null, connection, pStatement);
        }
        return (int) result;
    }

    /**
     * 查詢(非SELECT 會回傳自動累加識別碼)
     */
    public int ExecuteNonQueryWithRetrunID(String sqlquery, List<Object> paramList) throws Exception {
        InitPool();
        Connection connection = null;
        PreparedStatement pStatement = null;
        int resultid = -1;

        try {
            connection = datasource.getConnection();
            pStatement = connection.prepareStatement(sqlquery, Statement.RETURN_GENERATED_KEYS);
            System.out.println("執行的SQL語法：" + sqlquery);

            Execute(paramList, true, pStatement);
            if (pStatement.getGeneratedKeys().wasNull() == false) {
                ResultSet generatedKeys = pStatement.getGeneratedKeys();
                if (generatedKeys.next()) {
                    resultid = generatedKeys.getInt(1);
                }
            } else
                throw new Exception("無回傳的自動累加識別碼，請確認該表有自動累加識別碼");
        } catch (SQLException e) {
            if (e.getErrorCode() == 0 && !isInitializing) {
                datasource = null;
                InitPool();
            }
            e.printStackTrace();
            throw e;
        } finally {
            Close(null, connection, pStatement);
        }
        return resultid;
    }

    /**
     * 批次查詢(非SELECT)
     */
    public List<Integer> ExecuteNonQueryBatch(String sqlquery, List<List<Object>> paramList_Batch) throws Exception {
        long startTime = System.currentTimeMillis();
        List<Integer> results = new ArrayList<>();
        InitPool();
        Connection connection = null;
        PreparedStatement pStatement = null;
        try {
            connection = datasource.getConnection();
            pStatement = connection.prepareStatement(sqlquery);
            System.out.println("執行的SQL語法：" + sqlquery);
            for (List<Object> batchParam : paramList_Batch) {
                int index = 1;
                for (Object param : batchParam) {
                    SetStatementParam(pStatement, index, param);
                    index++;
                }
                pStatement.addBatch();
            }
            connection.setAutoCommit(false);
            int[] resultArray = pStatement.executeBatch();
            results = Arrays.stream(resultArray).boxed().collect(Collectors.toList());

        } catch (Exception e) {
            if (connection != null)
                connection.rollback();
            e.printStackTrace();
            throw e;
        } finally {
            if (connection != null) {
                connection.commit();
                connection.setAutoCommit(true);
            }
            Close(null, connection, pStatement);
        }
        long endTime = System.currentTimeMillis();
        long totTime = endTime - startTime;
        System.out.println("SQL執行時間:" + totTime);
        return results;
    }

    private Object Execute(List<Object> paramList, Boolean isNonQuery, PreparedStatement pStatement) throws Exception {
        Object result = null;
        int index = 1;
        for (Object sql_Parameter : paramList) {
            SetStatementParam(pStatement, index, sql_Parameter);
            index++;
        }
        if (isNonQuery)
            result = pStatement.executeUpdate();
        else
            result = pStatement.executeQuery();
        return result;
    }

    /**
     * 設定參數的型別
     */
    private void SetStatementParam(PreparedStatement pStatement, int index, Object sql_Parameter) throws SQLException {
        if (sql_Parameter != null) {
            Class<? extends Object> type = sql_Parameter.getClass();
            if (type == Integer.class)
                pStatement.setInt(index, Integer.valueOf(sql_Parameter.toString()));
            else if (type == String.class)
                pStatement.setString(index, sql_Parameter.toString());
            else if (type == Double.class)
                pStatement.setDouble(index, Double.valueOf(sql_Parameter.toString()));
            else if (type == Boolean.class)
                pStatement.setBoolean(index, Boolean.valueOf(sql_Parameter.toString()));
            else if (type == Date.class)
                pStatement.setTimestamp(index, new java.sql.Timestamp(((Date) sql_Parameter).getTime()));
            else if (type == Float.class)
                pStatement.setFloat(index, Float.valueOf(sql_Parameter.toString()));
            else if (type == Short.class)
                pStatement.setShort(index, Short.valueOf(sql_Parameter.toString()));
            else if (type == BigDecimal.class)
                pStatement.setBigDecimal(index, BigDecimal.valueOf(Double.valueOf(sql_Parameter.toString())));
            else if (type == ByteArrayInputStream.class)
                pStatement.setBinaryStream(index, (ByteArrayInputStream)sql_Parameter);
        } else {
            // pStatement.setObject(index, sql_Parameter);
            pStatement.setObject(index, sql_Parameter, Types.NVARCHAR);
        }
    }

    /**
     * 設定IN參數的型別
     */
    private void SetCallableStatementParam(CallableStatement cStatement, int index, Object sql_Parameter)
            throws SQLException {
        if (sql_Parameter != null) {
            Class<? extends Object> type = sql_Parameter.getClass();
            if (type == Integer.class)
                cStatement.setInt(index, Integer.parseInt(sql_Parameter.toString()));
            else if (type == String.class)
                cStatement.setString(index, sql_Parameter.toString());
            else if (type == Double.class)
                cStatement.setDouble(index, Double.parseDouble(sql_Parameter.toString()));
            else if (type == Boolean.class)
                cStatement.setBoolean(index, Boolean.parseBoolean(sql_Parameter.toString()));
            else if (type == Date.class)
                cStatement.setTimestamp(index, new java.sql.Timestamp(((Date) sql_Parameter).getTime()));
            else if (type == Float.class)
                cStatement.setFloat(index, Float.valueOf(sql_Parameter.toString()));
            else if (type == Short.class)
                cStatement.setShort(index, Short.valueOf(sql_Parameter.toString()));
            else if (type == BigDecimal.class)
                cStatement.setBigDecimal(index, BigDecimal.valueOf(Double.valueOf(sql_Parameter.toString())));
        } else
            cStatement.setObject(index, sql_Parameter);
    }

    /**
     * 設定OUT參數的型別
     */
    private void SetCallableStatementParam(CallableStatement cStatement, int index, Integer types) throws SQLException {
        if (types != null)
            cStatement.registerOutParameter(index, types);
        else
            cStatement.registerOutParameter(index, Types.OTHER);
    }

    /**
     * 取得OUT參數的型別
     */
    private Object GetCallableStatementParam(CallableStatement cStatement, int index, Integer types)
            throws SQLException {
        if (types != null) {
            if (types == Types.INTEGER)
                return cStatement.getInt(index);
            else if (types == Types.VARCHAR)
                return cStatement.getString(index);
            else if (types == Types.DOUBLE)
                return cStatement.getDouble(index);
            else if (types == Types.BOOLEAN)
                return cStatement.getBoolean(index);
            else if (types == Types.DATE)
                return cStatement.getTimestamp(index);
            else if (types == Types.FLOAT)
                return cStatement.getFloat(index);
            else if (types == Types.SMALLINT)
                return cStatement.getShort(index);
            else if (types == Types.NUMERIC)
                return cStatement.getBigDecimal(index);
        }
        return cStatement.getObject(index);
    }

    /**
     * 關閉連線相關
     */
    private void Close(ResultSet resultSet, Connection connection, PreparedStatement pStatement) throws SQLException {
        if (pStatement != null)
            pStatement.close();
        if (resultSet != null)
            resultSet.close();
        if (connection != null)
            connection.close();

        resultSet = null;
        connection = null;
        pStatement = null;
    }
}
package TransferData;

public class DB_Operate_CTMS_Center extends DB_Operate_MS {
    private final String useName = "CTMS_Center";
    private static DB_Operate_CTMS_Center db_Operate_CTMS_Center = null;
    private static final Object syncRoot = new Object();

    private DB_Operate_CTMS_Center() throws Exception {
        super();
    }

    public static DB_Operate_CTMS_Center GetInstance() throws Exception {
        if (db_Operate_CTMS_Center == null) {
            synchronized (syncRoot) {
                if (db_Operate_CTMS_Center == null) {
                    db_Operate_CTMS_Center = new DB_Operate_CTMS_Center();
                }
            }
        }
        return db_Operate_CTMS_Center;
    }

    @Override
    protected String GetUseName() {
        return useName;
    }

}
package TransferData;

public abstract class DB_Operate_MS extends DB_Operate_MS_Base {

    private final static String DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";

    protected abstract String GetUseName();

    public DB_Operate_MS() throws Exception {
        super(DRIVER);
    }

    protected String GetConnectString() throws Exception {
        DB_Infomation information = new DB_Infomation();
        information.ipAddress = "103.138.93.127";
        information.port = "";
        information.loginID = "GPS";
        information.password = "asdf1234";
        information.dbName = "TPF_IN";
//
//        information.ipAddress = "10.1.13.10";
//        information.port = "1433";
//        information.loginID = "EupUser";
//        information.password = "EupFin168";
//        information.dbName = "CTMS_Center";

        String connectString = "jdbc:sqlserver://" + information.ipAddress + ";" + "user=" + information.loginID + ";"
                + "password=" + information.password + ";" + "databasename=" + information.dbName + ";Max Pool Size=200;";
        return connectString;
    }
}
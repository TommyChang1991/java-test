package TransferData;

import com.eup.fms.base.lib.component.Eup_DateTime;
import com.eup.fms.base.lib.extension_methods.Eup_Objects;
import com.eup.fms.dao.table.cassandra.eup_log.Operate_Eup_Log_Factory;
import com.eup.fms.dao.table.ms.ctms_statis.Operate_CTMS_Statis_Factory;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TransferDataTest {
    public static void main(String[] args) {
        try {
            String unicode = "171488";
            String startTime = "2022-02-18 13:44:00";
            String endTime = "2022-02-18 14:28:10";
//            String unicode = "47136";
//            String startTime = "2022-03-03 00:00:00";
//            String endTime = "2022-03-03 23:59:59";
            List<Map<String, Object>> rawDataList = Operate_Eup_Log_Factory.getInstance().getNew_logq().getLogqDataNew(unicode, startTime, endTime, 8);
            System.out.println("rawDataList.size() : " + rawDataList.size());

//            System.out.println(gisToDegMin("x", "120120316"));
//            System.out.println(gisToDegMin("y", "23192138"));

            DecimalFormat df = new DecimalFormat("000");
            List<Map<String, String>> newDataList = new ArrayList<>();
            for (Map<String, Object> r: rawDataList) {
                Map<String, String> newData = new HashMap<>();
                newData.put("dt", new Eup_DateTime(r.get("Log_DTime").toString()).getDateTimeString("yyyy-MM-dd HH:mm:ss.000"));
                newData.put("x", gisToDegMin("x", Eup_Objects.toString(r.get("Log_GISX"))));
                newData.put("y", gisToDegMin("y", Eup_Objects.toString(r.get("Log_GISY"))));
                newData.put("speed", df.format(Eup_Objects.toDouble(r.get("Log_Speed"))));
                newData.put("deg", df.format(Eup_Objects.toDouble(r.get("Log_Direct"))));
                newData.put("valid", Eup_Objects.toString(r.get("Log_Status")));
                newData.put("IO1", Eup_Objects.toString(r.get("Log_Warm")).charAt(5) == '0' ? "0" : "1");
                newDataList.add(newData);
            }

//            int resultCount = 0;
//            Eup_DateTime t1 = new Eup_DateTime();
//            for (int i = 0; i < 100; i++) {
//                String insertTest = "INSERT INTO tb_limit_query_new_car (cust_id, unicode, query_id) VALUES (?, ?, ?)";
//                List<Object> paramList = Arrays.asList(3000001, 153257, i);
//                int result = DB_Operate_CTMS_Center.GetInstance().ExecuteNonQuery(insertTest, paramList);
//                resultCount += result;
//            }
//            Eup_DateTime t2 = new Eup_DateTime();
//            System.out.println("t2 - t1 : " + t2.Subtract(t1).GetSecond() + " sec");
//            System.out.println("resultCount : " + resultCount);
            int resultCount = 0;
            String insertSqlQuery =
                    "INSERT INTO ttCarImport(car_licence, dt, x, y, speed, deg, valid, dept_id, IO1) "
                            + "VALUES('KLB-5377', ?, ?, ?, ?, ?, ?, '16642135', ?)";
            Eup_DateTime t1 = new Eup_DateTime();
            for (Map<String, String> newData : newDataList) {
                List<Object> paramList = new ArrayList<>();
                paramList.add(newData.get("dt"));
                paramList.add(newData.get("x"));
                paramList.add(newData.get("y"));
                paramList.add(newData.get("speed"));
                paramList.add(newData.get("deg"));
                paramList.add(newData.get("valid"));
                paramList.add(newData.get("IO1"));
                int result = DB_Operate_CTMS_Center.GetInstance().ExecuteNonQuery(insertSqlQuery, paramList);
                if (result > 1) {
                    Operate_CTMS_Statis_Factory.GetInstance().getErrorLog()
                            .InsertErrorLog("Complement_Forward", "Complement_Forward", "insert failed, dt : " + newData.get("dt"));
                }
                resultCount += result;
                System.out.println(resultCount);
            }
            Eup_DateTime t2 = new Eup_DateTime();
            System.out.println("t2 - t1 : " + t2.Subtract(t1).GetSecond() + " sec");
            System.out.println("Done, total resultCount : " + resultCount);

            String resultSql = "SELECT * FROM ttCarImport WHERE car_licence = 'KLB-5377' AND dt >= '2022-02-18 13:44:00' AND dt <= '2022-02-18 14:28:10' ";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String gisToDegMin(String xy, String gis) {
        DecimalFormat gisDf = new DecimalFormat("00.0000");
        if (xy.equals("x")) {
            Double min = Double.parseDouble("0." + gis.substring(3, gis.length())) * 60;
            return gis.substring(0, 3) + gisDf.format(min);
        } else {
            Double min = Double.parseDouble("0." + gis.substring(2, gis.length())) * 60;
            return gis.substring(0,2) + gisDf.format(min);
        }
    }
}
